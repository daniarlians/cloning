import React from 'react'
import {View, Image,Text,StyleSheet, TouchableOpacity} from 'react-native'
import  Icon  from 'react-native-vector-icons/Feather'
import  Icon1  from 'react-native-vector-icons/Ionicons'
import  Icon2  from 'react-native-vector-icons/Ionicons'
import  Icon3  from 'react-native-vector-icons/Ionicons'
import {useState} from 'react'



const Halaman = () => {
    const [state, setstate] = useState (0)
    return (
        <View>
            <View style={style.logo3}>
            <Image style={{width:35,height:35,marginRight:15,marginTop:10,borderRadius:20,marginVertical:10,marginHorizontal:10,borderColor:'red',borderWidth:2, marginBottom:2}} source={require('../assets/4.jpg')}/>
            <Text style={{fontWeight:'bold',marginVertical:18,minWidth:30,}}>Hendra_kusumo</Text>
            <Icon3 style={style.Icon3} name="ellipsis-vertical" size={25}/>
            </View>
            <Image source={require('../assets/dw.jpg')}/>
            <View style={style.logo3}>
            <TouchableOpacity>
            <Icon style={style.navBawah} name="heart" size={30} onPress={() => setstate(state + 1)}/>
            </TouchableOpacity>
            <Icon1 style={style.navBawah} name="ios-chatbubble-ellipses-outline" size={30}/>
            <Icon2 style={style.navBawah} name="paper-plane-outline" size={30}/>
            <Icon3 style={style.logo4} name="bookmarks-outline" size={30}/>
            </View>
            <View style={style.baca}>
            <Text style={{fontWeight:'bold',marginHorizontal:5}}>{state} suka</Text>
            <Text style={{fontSize:14,fontWeight: '900',marginHorizontal:5}}>yang gagal itu usahamu, bukan hidupmu, yang dihancurkan hanya rencanamu bukan masa depanmu.</Text>
            <Text style={{fontSize:11,fontWeight:'normal',marginVertical:5,minWidth:30,color:'grey'}}>   2 hari yang lalu</Text>
            </View>  
        </View>
        )
    }
    
    const style= StyleSheet. create({
        Icon3: {
            flexDirection: 'row-reverse',
            marginHorizontal:165,
            marginVertical:10
        },
        logo1: {
            flexDirection:'row',
            backgroundColor:'white',
            elevation:1
        },
        navItem: {
            marginHorizontal:10,
            marginVertical:8
        },
        logo4: {
            marginHorizontal:165,
            marginVertical:8
        },
        text: {
            height:100,
            backgroundColor:'white',
            flexDirection:'column',
        },
        logo3: {
            height:55,
            flexDirection:'row',
            backgroundColor:'white',
        },
        navBawah: {
            marginHorizontal:10,
            marginVertical:8
        },
        baca: {
            height:100,
            backgroundColor:'white',
            flexDirection:'column',
        }
    });
    
    export default Halaman
    