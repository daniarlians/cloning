import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import {Header} from './index';
import {Story} from './index';
import {Conten} from './index';
import {Conten22} from './index';
import {Conten33} from './index';
import {Conten44} from './index';
import {Conten55} from './index';

import Footer from './Footer'; 

const App = () => {
    return (
        <View>
            <Header />
            <ScrollView>
                <Story />
                <Conten />
                <Conten22 />
                <Conten33 />
                <Conten44 />
                <Conten55 />

            </ScrollView>
            <Footer />
        </View>

        
    )
}

export default App
