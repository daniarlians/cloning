import React from 'react';
import {View, Text, ScrollView, Image,} from  'react-native';

const Story =() => {
    return (
        <View >
            <ScrollView horizontal={true}
            showsHorizontalScrollIndicator={false} style={{height:110,backgroundColor: 'white',paddingHorizontal:15}}>
                <View>
                <Image source={require('../assets/cb.jpg')} 
                style={{height: 75,width: 100,borderRadius:60,marginTop:17,marginHorizontal:8}} />
                <Text style={{fontSize:10,fontWeight:'normal',marginVertical:5,minWidth:30,marginTop:0, marginLeft: 24}}> Cerita Anda</Text>
                </View>
                <View>
                <Image source={require('../assets/b.jpg')} 
                style={{height: 70,width: 70,borderRadius:60,marginTop:20,marginHorizontal:1,borderColor:'red',borderWidth:2, marginBottom:2}} />
                <Text style={{fontSize:10,fontWeight:'normal',marginVertical:5,minWidth:30,marginTop:0, marginLeft: 20}}> Diaz fz</Text>
                </View>
                <View>
                <Image source={require('../assets/c.jpg')} 
                style={{height: 70,width: 70,borderRadius:60, marginTop:20,marginHorizontal:15,borderColor:'red',borderWidth:2, marginBottom:2}} />
                <Text style={{fontSize:10,fontWeight:'normal',marginVertical:5,minWidth:30,marginTop:0, marginLeft: 15}}> Mahrus_121</Text>
                </View>
                <View>
                <Image source={require('../assets/f.jpg')} 
                style={{ height: 70,width: 70, borderRadius:60,marginTop:20,marginHorizontal:1,borderColor:'red',borderWidth:2, marginBottom:2}} />
                <Text style={{fontSize:10,fontWeight:'normal',marginVertical:5,minWidth:30,marginTop:0, marginLeft: 1}}> dani_ozvaldo</Text>
                </View>
                <View>
                <Image source={require('../assets/g.jpg')} 
                style={{height: 70,width: 70,borderRadius:60, marginTop:20,marginHorizontal:13,borderColor:'red',borderWidth:2, marginBottom:2}} />
                <Text style={{fontSize:10,fontWeight:'normal',marginVertical:5,minWidth:30,marginTop:0, marginLeft: 18}}> Fawaid_we</Text>
                </View>
                <View>
                <Image source={require('../assets/h.jpg')} 
                style={{height: 70,width: 70, borderRadius:60, marginTop:20,marginHorizontal:8,borderColor:'red',borderWidth:2, marginBottom:2}} />
                <Text style={{fontSize:10,fontWeight:'normal',marginVertical:5,minWidth:30,marginTop:0, marginLeft: 14}}> Rendy_179</Text>
                </View>
                <View>
                <Image source={require('../assets/a.jpg')} 
                style={{height: 70,width: 70, borderRadius:60, marginTop:20,marginHorizontal:7,borderColor:'red',borderWidth:2, marginBottom:2}} />
                <Text style={{fontSize:10,fontWeight:'normal',marginVertical:5,minWidth:30,marginTop:0, marginLeft: 19}}> Fiqi bajil</Text>
                </View>
            </ScrollView>
        </View>
    )
}

export default Story